import { Component } from '@angular/core';
import { Calculadora } from '../models/Calculadora';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  calculadora: Calculadora;
  resp: number;
  constructor() {
    this.calculadora = new Calculadora();
  }

  private calcular(operacao: string){
    this.calculadora.operacao = operacao;
    this.resp = this.calculadora.calcular();
  }
}
